# Pronote Fetcher

In its current state, the script supports Pronote 2017 (See commit history for older versions of the script).
This is a phantomjs script. You need to have phantomjs installed in order to run it.

## Usage

Pronote url has to be given through the `PRONOTE_URL` environment variable.
```bash
export PRONOTE_URL="https://demo.index-education.net/pronote/mobile.eleve.html?fd=1"
```

Run it with
```bash
phantomjs app.js login password
```
To test with demo, use
```bash
phantomjs app.js demonstration pronotevs
```

## Vanilla scripts

Vanilla scripts are script meant to be run directly in the browser, to run without phantomjs
