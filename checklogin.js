/* eslint-disable no-undef */
'use strict';

var system = require('system');
var username = system.args[1] || 'demonstration';
var password = system.args[2] || 'pronotevs';
var url = (username === 'demonstration' && password === 'pronotevs') ? 'https://demo.index-education.net/pronote/mobile.eleve.html?fd=1' : (system.env.PRONOTE_URL);

var page = require('webpage').create();

function waitFor(testFx, onReady, timeOutMillis) {
    var maxtimeOutMillis = timeOutMillis ? timeOutMillis : 5000,
        start = new Date().getTime(),
        condition = false,
        interval = setInterval(function() {
            if ((new Date().getTime() - start < maxtimeOutMillis) && !condition) {
                condition = testFx();
            } else {
                if (!condition) {
                    // console.error('"waitFor()" timeout');
                    phantom.exit(1);
                } else {
                    // console.error('"waitFor()" finished in ' + (new Date().getTime() - start) + 'ms.');
                    setTimeout(onReady, 100);
                    clearInterval(interval);
                }
            }
        }, 300);
}

console.error = function () {
    require('system').stderr.write(Array.prototype.join.call(arguments, ' ') + '\n');
};

function login(username, password) {
    var inputs = document.getElementsByTagName('input');
    inputs[0].value = username;
    inputs[1].value = password;
    GInterface.traiterEvenementValidation();
}

function checkLoginInputs() {
    var inputs = document.getElementsByTagName('input');
    return inputs.length >= 2;
}

function checkLoggedIn() {
    return document.getElementsByClassName('WidgetTAF').length > 0;
}

// Open Pronote,
page.open(url, function(status) {
    if (status === 'success') {
        waitFor(function() {
            // Wait for input elements to be loaded
            return page.evaluate(checkLoginInputs);
        }, function() {
            console.error('Form loaded, logging in "' + username + '"');
            page.evaluate(login, username, password);
            waitFor(function() {
                var correct = page.evaluate(checkLoggedIn);
                var incorrect = page.evaluate(function () {
                    return document.getElementsByTagName('body')[0].textContent.indexOf('incorrect') >= 0;
                });
                return incorrect || correct;
            }, function() {
                console.log(page.evaluate(checkLoggedIn));
                phantom.exit(0);
            });
        });
    } else {
        console.error('Failed to load webpage');
        phantom.exit(1);
    }
});
